/**
 * CLASE: Test2.java
 * OBJETIVO: Prueba de la Clase PaginaWeb y Usuario
 * ASIGNATURA: 
 * @version 1.0 21/07/2005
 * @author William Mendoza Rodrguez
 */

public class Test2 {

    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/
    
    /** 
     * Muestra una cadena en pantalla.
     * PRE: TRUE
     * @param s El string a mostrar
     */
    
    public static void show(String s) { 
	System.out.println(s);
    }

    /**
     * Un test de la clase. Retorna un string.
     * PRE: TRUE 
     * POS: Crea tres objetos de tipo Usuario, junto con una PaginaWeb, los conecta  a dos de ellos y presenta sus datos. Despues desconecta dos y la vuelve a presentar 
     * @return String con el texto de la Pagina Web y sus Usuarios.
     */
    public static String test() {
	String s="";
	
	Usuario p1, p2, p3;
	PaginaWeb site;
	// Se crean dos objetos de tipo Persona
	p1 = new Usuario("is101711","mipass");
	p2 = new Usuario("is101056","aquenoadivinas");
	p3 = new Usuario("is101060","is101060");
	site = new PaginaWeb("www.yahoo.com");
	
	site.registrar(p1);
	site.registrar(p2);
	site.registrar(p3);

	site.logIn(p1);
	site.logIn(p2);
	s = site.toString();
	s += "\n";

	site.logOut(p2);
	site.logOut(p3);
	s += "Despues de los cambios: \n";
	s += site.toString();
	s += "Usuarios en línea: \n";
	s += site.usuariosEnLinea();
	return s;
    }
    
    /*=========================================================================
      PROGRAMA PRINCIPAL 
      =========================================================================*/
    /**
     * Metodo main para el test.
     */
    public static void main(String argv[]) {
	show(test());
    }
    		
}
