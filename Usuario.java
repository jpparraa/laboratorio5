/**
 * CLASE: Usuario.java
 * OBJETIVO: Clase que describe un Usuario de una Pagina Web con username y password
 * ASIGNATURA: 
 * @version 1.0 21/07/2005
 * @author William Mendoza Rodrguez
 */

public class Usuario {
    
    /*=========================================================================
      ATRIBUTOS PRINCIPALES
      =========================================================================*/

    private String username; /** nombre de usuario */
    private String password; /** palabra clave */
    
    
    /*=========================================================================
      METODOS CONSTRUCTORES
      =========================================================================*/
    
    /** 
     * Inicializa una instancia "vacia" de un nuevo objeto Usuario. Inicializando el username y password con un caracter vacio.
     */
    public Usuario() {
	username = "";
	password = "";
    }
    
    /**
     *
     * Inicializa un objeto de tipo Usuario.
     *
     * @param username Nombre de usuario.
     * @param password Contrasea.
     *
     */

    public Usuario(String username, String password){
	    this.username = username;
	    this.password = password;
    }

    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/
    
    /** 
     * Retorna el valor del atributo username. El valor es retornado en un objeto String.
     * PRE: TRUE
     * @return El valor del atributo nombre. En caso de no haber sido asignado retorna caracter vacio.
     */    
    
    public String getUsername() {
	return this.username;
    }
    
    /**
     *
     * Retorna la contrasea.
     *
     * @return password.
     *
     */

    public String getPassword(){
	    return this.password;
    }

    /**
     *
     * Valida la contrasa de un usuario.
     *
     * @param password Contrasea.
     *
     * @return true en caso de ser correcto. En caso contrario, false.
     *
     */

    public boolean passwordValido(String pass){
	return this.password.equals(pass);
    }
    /**
     * Metodo que valida que el nombre de usuario es identico al parametro user. 
     * PRE: TRUE
     * @param El username a validar
     * @return true en caso de ser correcto, en caso contrario false
     */	
    
    public boolean usernameValido(String user) {
	return username.equals(user);
    }
    
    /**
     * Obtiene en String los datos del usuario. No presentar su palabra clave
     * PRE: TRUE
     * @return los datos del usuario en String
     */
    
    public String toString() {
	String s = "";
	s += "\n Username: " + getUsername();
	return s;
    }
}



