import java.util.Vector; /** Utilidad para manejo de la clase Vector*/

/**
 * CLASE: PaginaWeb.java
 * OBJETIVO: Clase que describe un sitio en Internet con direccion electronica (URL)
 * ASIGNATURA: 
 * @version 1.0 21/07/2005
 * @author William Mendoza Rodriguez
 */

public class PaginaWeb {
    
    /*=========================================================================
      ATRIBUTOS PRINCIPALES
      =========================================================================*/
    private String direccionElectronica; /** Direccion electronica de la pagina (URL)*/
    private Vector usuariosRegistrados; /** Vector de usuarios registrados al ssitio */
    private Vector usuariosActuales; /** Vector de usuarios conectados en el sitio */
    
    /*=========================================================================
      METODOS CONSTRUCTORES
      =========================================================================*/
    /** 
     * Inicializa una instancia "vacia" de un nuevo objeto PaginaWeb. Inicializando la direccion electronica con un caracter vacio. Los vectores son inicializados tambien en "vacio".
     */

    public PaginaWeb() {
		this.direccionElectronica="";
		this.usuariosRegistrados= new Vector();
		this.usuariosActuales = new Vector();
    }




    public PaginaWeb(String url) {
		this.direccionElectronica = url;
		this.usuariosRegistrados= new Vector();
		this.usuariosActuales = new Vector();
    }



    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/
    
    /** 
     * Asigna el valor de la direccion electronica de la PaginaWeb 
     * PRE: Que exista el objeto
     * POS: this.direccionElectronica == url
     * @param La direccion electronica de la pgina web
     */

    public void setDireccionElectronica(String url) {
		this.direccionElectronica = url;
    }

    /** 
     * Registra un usuario en la pgina.
     * PRE: Que exista la PaginaWeb
     * POS: usuariosRegistrados.size() == tamAnterior+1
     * @param Usuario a registrar.
     */
    
    public void registrar(Usuario p) {
	usuariosRegistrados.add(p);
    }

    /** 
     * La direccion electronica de la Pagina Web.
     * PRE: TRUE
     * @return El url de la Pagina Web en String. En caso de no haber sido asignado retorna ""
     */
    
    public String getDireccionElectronica() {
	return direccionElectronica;
    }

    /** 
     * Numero de usuarios que estn conectados en la Pgina.
     * PRE: TRUE
     * @return Numero de usuarios que estn conectados actualemente en la Pgina. En caso de no haber ninguno retorna 0.
     */
    
    public int numeroUsuariosActuales() {
	return usuariosActuales.size();
    }

    /** 
     * Numero de usuarios que estn registrados en la Pgina.
     * PRE: TRUE
     * @return Numero de usuarios registrados en la Pgina. En caso de no haber ninguno retorna 0.
     */   
    
    public int numeroUsuariosRegistrados() {
		return usuariosRegistrados.size();
    }
    
    /**
     *
     * Retorna los usuarios registrados.
     *
     * @return usuarios registrados.
     *
     */

    public String usuariosRegistrados(){
	String s = "";
	for(int i = 0; i < usuariosRegistrados.size(); i++){
		s += usuariosRegistrados.elementAt(i).toString() + '\n';
	}
	return s;
    }

    /**
     * Metodo que registra la salidad de un usuario (desconectarse) del sitio. Inidca si esta fue exitosa o no.
     * PRE: Que el usuario a desconectar
     * @param El usuario a desconectar
     * @return true en caso de poderse desconectarse, en caso contrario false. Tambien retorna false si el usuario no estaba en linea
     */
    
    
    public boolean logIn(Usuario p) {

		Usuario userTemp;

		boolean existe=false;
			int i=0;
		
		
			while(i<numeroUsuariosRegistrados() && existe==false ){
				
				
				userTemp=(Usuario)usuariosRegistrados.elementAt(i);

				if(  userTemp.usernameValido(p.getUsername())    ){
					
													
					if(  userTemp.passwordValido( p.getPassword())   ){
						
						usuariosActuales.add(p);
						existe=true;
												
					}
					

			
						
					
					else
						System.out.println("El password no es valido");
					
				}
				
			i++;
				
			}
				

				

					
					
					
					
					


	return existe;
    }    
    
    
    /**
     *
     * Elimina un usuario de usuariosActuales (Log out).
     *
     * @param p Usuario.
     *
     * @return true si el proceso fue exitoso. En caso contrario, false.
     *
     */
    
    public boolean logOut(Usuario p) {
	    return usuariosActuales.remove(p);
    }

    /** 
     * Obtiene los el username de los usuarios conectados.
     * PRE: Deben existir usuarios en linea
     * @return Los usernames de los usuarios conectados. En caso contrario regresa el caracter '-'
     */
    public String usuariosEnLinea() {
	String s = "";
	Usuario user;
	for (int i=0; i<numeroUsuariosActuales(); i++) {
	    //REALICE LO NECESARIO PARA MOSTRAR A LOS USUARIOS ACTUALES
	    s += usuariosActuales.elementAt(i).toString() + '\n';
	}
	return s;
    }
	
    /**
     * Obtiene en String los datos de la Pagina Web. 
     * PRE: TRUE
     * @return Presenta la direccion electronica, el numero de usuarios registrados, el numero total de usuarios, y los username de estos
     */
    public String toString() {
	String s = "";
	s += "URL: " + getDireccionElectronica() + "\n";
	s += "Usuarios Registrados: " + numeroUsuariosRegistrados() + "\n";
	s += "Usuarios Conectados: " + numeroUsuariosActuales() + "\n";
	return s;
    }
}
