/**
 * CLASE: Test1.java
 * OBJETIVO: Prueba de la Clase PaginaWeb y Usuario
 * ASIGNATURA: 
 * @version 1.0 21/07/2005
 * @author William Mendoza Rodriguez
 */

public class Test1 {
    
    /*=========================================================================
      DEFINICION E IMPLEMENTACION DE LOS METODOS ANALIZADORES Y MODIFICADORES
      =========================================================================*/
    
    /** 
     * Muestra una cadena en pantalla.
     * PRE: TRUE
     * @param s El string a mostrar
     */
    
    public static void show(String s) { 
	System.out.println(s);
    }

    /**
     * Un test de la clase. Retorna un string.
     * PRE: TRUE 
     * POS: Crea dos objetos de tipo Usuario, junto con una PaginaWeb, los registra y presenta sus datos
     * @return String con el texto de la Pagina Web y sus Usuarios.
     */
    public static String test() {
	String s="";
	
	Usuario p1, p2;
	PaginaWeb site;
	// Se crean dos objetos de tipo Persona
	p1 = new Usuario("is101711","mipass");
	p2 = new Usuario("is101056","aquenoadivinas");
	site = new PaginaWeb();
	site.setDireccionElectronica("www.yahoo.com");
	
	site.registrar(p1);
	site.registrar(p2);
	s = site.toString();
	s += "\n";
	
	s += "Despues de los cambios: \n";
	s += site.toString();
	s += "\nUsuarios Registrados\n";
	s += site.usuariosRegistrados();
	return s;
    }
    
    /*=========================================================================
      PROGRAMA PRINCIPAL 
      =========================================================================*/
    /**
     * Metodo main para el test.
     */
    
    public static void main(String argv[]) {
	show(test());
    }
    
}
